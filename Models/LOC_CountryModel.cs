﻿using System.ComponentModel.DataAnnotations;

namespace AddressBook.Models
{
    public class LOC_CountryModel
    {
        public int ?CountryID { get; set; }
        [Required]
        public string CountryName { get; set; }
        public string CountryCode { get; set; }

        public DateTime? Created { get; set; }
        public DateTime? Modified { get; set; }

    }
}
