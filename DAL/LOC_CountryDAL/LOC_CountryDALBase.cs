﻿using Microsoft.Practices.EnterpriseLibrary.Data.Sql;
using System.Data;
using System.Data.Common;

namespace AddressBook.DAL.LOC_CountryDAL
{
    public class LOC_CountryDALBase : DALHelper
    {
        #region dbo_PR_LOC_Country_SelectAll
        public DataTable dbo_PR_LOC_Country_SelectAll()
        {
            try
            {
                SqlDatabase sqlDatabase = new SqlDatabase(myConnectionString);
                DbCommand dbCMD = sqlDatabase.GetStoredProcCommand("PR_LOC_Country_SelectAll");
                DataTable dt = new DataTable();
                using (IDataReader reader = sqlDatabase.ExecuteReader(dbCMD))
                {
                    dt.Load(reader);
                }
                return dt;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }
}
