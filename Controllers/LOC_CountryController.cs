﻿using AddressBook.DAL.LOC_CountryDAL;
using AddressBook.Models;
using Microsoft.AspNetCore.Mvc;
using System.Data;
using System.Data.SqlClient;

namespace AddressBook.Controllers
{
    public class LOC_CountryController : Controller
    {

        LOC_CountryDAL dalLOC_CountryDAL=new LOC_CountryDAL();

        private IConfiguration Configuration;
        public LOC_CountryController(IConfiguration _configuration)
        {
            Configuration = _configuration;
        }
        public IActionResult Index()
        {
            DataTable dt = dalLOC_CountryDAL.dbo_PR_LOC_Country_SelectAll();
            return View("LOC_CountryList",dt);
        }
        //public IActionResult Delete(int CountryID)1
        //{
        //    string str = this.Configuration.GetConnectionString("myConnectionString");
        //    SqlConnection conn = new SqlConnection(str);
        //    conn.Open();
        //    SqlCommand cmd = conn.CreateCommand();
        //    cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //    cmd.CommandText = "PR_LOC_Country_Delete";
        //    //cmd.Parameters.AddWithValue("CountryID");
        //    cmd.ExecuteNonQuery();
        //    conn.Close();
        //    return RedirectToAction();
        //}

        public IActionResult AddEditCountryForm()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Save(LOC_CountryModel modelLOC_CountryModel)   
        {   
            string connectionstr = this.Configuration.GetConnectionString("myConnectionString");
            //PrePare a connection
            SqlConnection conn = new SqlConnection(connectionstr);
            conn.Open();

            //Prepare a Command
            SqlCommand objCmd = conn.CreateCommand();
            objCmd.CommandType = CommandType.StoredProcedure;
            if (modelLOC_CountryModel.CountryID == null)
            {
                objCmd.CommandText = "PR_LOC_Country_Insert";
                objCmd.Parameters.Add("@Created", SqlDbType.DateTime).Value = DBNull.Value;
            }
            else
            {
                objCmd.CommandText = "PR_LOC_Country_Update";
                objCmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = modelLOC_CountryModel.CountryID;
            }
            objCmd.Parameters.Add("@CountryName", SqlDbType.VarChar).Value = modelLOC_CountryModel.CountryName;
            objCmd.Parameters.Add("@CountryCode", SqlDbType.VarChar).Value = modelLOC_CountryModel.CountryCode;
            objCmd.Parameters.Add("@Modified", SqlDbType.DateTime).Value = DBNull.Value;

            if (Convert.ToBoolean(objCmd.ExecuteNonQuery()))
            {
                if (modelLOC_CountryModel.CountryID == null)
                    TempData["CountryInsertMsg"] = "Record Inserted Successfully";
                else
                    return RedirectToAction("Index");
            }
            conn.Close();
            return View("AddEditCountryForm");
        }

        public IActionResult Add(int? CountryID)
        {
            if (CountryID != null)
            {
                string connectionstr = this.Configuration.GetConnectionString("myConnectionString");
                //PrePare a connection
                SqlConnection conn = new SqlConnection(connectionstr);
                conn.Open();

                //Prepare a Command
                SqlCommand objCmd = conn.CreateCommand();
                objCmd.CommandType = CommandType.StoredProcedure;
                objCmd.CommandText = "PR_LOC_Country_SelectByPK";
                objCmd.Parameters.Add("@CountryID", SqlDbType.Int).Value = CountryID;
                DataTable dt = new DataTable();
                SqlDataReader objSDR = objCmd.ExecuteReader();
                dt.Load(objSDR);

                LOC_CountryModel modelLOC_Country = new LOC_CountryModel();

                foreach (DataRow dr in dt.Rows)
                {
                    modelLOC_Country.CountryID = Convert.ToInt32(dr["CountryID"]);
                    modelLOC_Country.CountryName = dr["CountryName"].ToString();
                    modelLOC_Country.CountryCode = dr["CountryCode"].ToString();
                }

                return View("AddEditCountryForm", modelLOC_Country);
            }
            return View("AddEditCountryForm");
        }
        public IActionResult Delete(int CountryID)
        {
            string str = this.Configuration.GetConnectionString("myConnectionString");
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand sqlCommand = conn.CreateCommand();
            sqlCommand.CommandType = CommandType.StoredProcedure;
            sqlCommand.CommandText = "PR_LOC_Country_Delete";
            sqlCommand.Parameters.AddWithValue("@CountryID", CountryID);
            sqlCommand.ExecuteNonQuery();
            conn.Close();
            return RedirectToAction("Index");
        }
    }
    }

