﻿    using AddressBook.DAL.LOC_CityDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AddressBook.Controllers
{
    public class LOC_CityController : Controller
    {
        LOC_CityDAL LOC_CityDAL=new LOC_CityDAL();
        public IActionResult Index()
        {
            DataTable dt=LOC_CityDAL.dbo_PR_LOC_City_SelectAll();
            return View("LOC_CityList",dt);
        }
    }
}
