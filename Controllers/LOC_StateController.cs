﻿using AddressBook.DAL.LOC_StateDAL;
using Microsoft.AspNetCore.Mvc;
using System.Data;

namespace AddressBook.Controllers
{
    public class LOC_StateController : Controller
    {
        LOC_StateDAL _stateDAL=new LOC_StateDAL();
        public IActionResult Index()
        {
            DataTable dt=_stateDAL.dbo_PR_LOC_State_SelectAll();
            return View("LOC_StateList",dt);
        }
    }
}
